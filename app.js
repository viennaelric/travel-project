//app.js

const http= require('http');

//create an instance of the http server to handle http requests
let app = http.createServer((req, res) => {
  //set a response type of the plain text for the response
  res.writeHead(200, {'Content-Type': 'text/plain'});

  //send back a response and end the connection
  res.end('Hello World!\n');
});

//start the server on port 3000
app.listen(3000, '127.0.0.1');
console.log('Node server runnning on port 3000');
